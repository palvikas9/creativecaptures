package com.creativecaptures.app.di.module;

import com.creativecaptures.app.ui.category.SingleCategoryActivity;
import com.creativecaptures.app.ui.home.CategoryFragmentBindingModule;
import com.creativecaptures.app.ui.home.HomeActivity;
import com.creativecaptures.app.ui.splash.SplashActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = {CategoryFragmentBindingModule.class})
    abstract HomeActivity bindHomeActivity();

    @ContributesAndroidInjector
    abstract SingleCategoryActivity bindSingleCategoryActivity();

    @ContributesAndroidInjector
    abstract SplashActivity bindSplashScreenActivity();

}
