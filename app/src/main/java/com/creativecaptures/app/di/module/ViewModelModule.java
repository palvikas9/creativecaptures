package com.creativecaptures.app.di.module;

import com.creativecaptures.app.di.util.ViewModelFactory;
import com.creativecaptures.app.di.util.ViewModelKey;
import com.creativecaptures.app.ui.category.SingleCategoryViewModel;
import com.creativecaptures.app.ui.home.AboutUsViewModel;
import com.creativecaptures.app.ui.home.CategoriesViewModel;
import com.creativecaptures.app.ui.home.SupportViewModel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CategoriesViewModel.class)
    abstract ViewModel bindCategoriesViewModel(CategoriesViewModel homeViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AboutUsViewModel.class)
    abstract ViewModel bindAboutViewModel(AboutUsViewModel homeViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SingleCategoryViewModel.class)
    abstract ViewModel bindSingleCategoryViewModel(SingleCategoryViewModel singleCategoryViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SupportViewModel.class)
    abstract ViewModel bindSupportViewModel(SupportViewModel supportViewModel);


    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
