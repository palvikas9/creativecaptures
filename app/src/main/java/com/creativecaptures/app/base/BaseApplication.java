package com.creativecaptures.app.base;

import com.creativecaptures.app.di.component.ApplicationComponent;
import com.creativecaptures.app.di.component.DaggerApplicationComponent;
import com.creativecaptures.app.ui.utils.FontHelper;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class BaseApplication extends DaggerApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        FontHelper.getInstance(getApplicationContext()).overrideFont();
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        ApplicationComponent component = DaggerApplicationComponent.builder().application(this).build();
        component.inject(this);

        return component;
    }
}

