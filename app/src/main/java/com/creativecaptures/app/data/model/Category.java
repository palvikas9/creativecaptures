package com.creativecaptures.app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Category implements Serializable {

    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("src_thumb")
    @Expose
    private String srcThumb;

    @SerializedName("src")
    @Expose
    private String src;

    @SerializedName("categorySlug")
    @Expose
    private String categorySlug;

    public long getId() {
        return id;
    }

    public Category setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Category setName(String name) {
        this.name = name;
        return this;

    }

    public String getSrcThumb() {
        return srcThumb;
    }

    public Category setSrcThumb(String srcThumb) {
        this.srcThumb = srcThumb;
        return this;

    }

    public String getSrc() {
        return src;
    }

    public Category setSrc(String src) {
        this.src = src;
        return this;

    }

    public String getCategorySlug() {
        return categorySlug;
    }

    public Category setCategorySlug(String categorySlug) {
        this.categorySlug = categorySlug;
        return this;
    }
}
