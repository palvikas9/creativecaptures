
package com.creativecaptures.app.data.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class Pages {

    @Expose
    private Page page;
    @Expose
    private String status;

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
