package com.creativecaptures.app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Images implements Serializable {

    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("src")
    @Expose
    private String src;

    @SerializedName("thumb")
    @Expose
    private String thumb;

    public long getId() {
        return id;
    }

    public Images setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Images setName(String name) {
        this.name = name;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public Images setCategory(String category) {
        this.category = category;
        return this;
    }

    public String getSrc() {
        return src;
    }

    public Images setSrc(String src) {
        this.src = src;
        return this;
    }

    public String getThumb() {
        return thumb;
    }

    public Images setThumb(String thumb) {
        this.thumb = thumb;
        return this;
    }
}
