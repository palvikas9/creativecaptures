package com.creativecaptures.app.data.rest;

import com.creativecaptures.app.data.model.Category;
import com.creativecaptures.app.data.model.Pages;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("http://creativecapture.co.in/api/get_page/")
    Single<List<Category>> getCategories();

    @GET("http://creativecapture.co.in/api/get_page/")
    Single<Pages> getPage(@Query("slug") String categorySlug);
}
