package com.creativecaptures.app.data.rest;

import com.creativecaptures.app.data.model.Category;
import com.creativecaptures.app.data.model.Pages;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class ApiRepository {

    private final ApiInterface apiInterface;

    @Inject
    public ApiRepository(ApiInterface repoService) {
        this.apiInterface = repoService;
    }

    public Single<List<Category>> getCategories() {
        return apiInterface.getCategories();
    }

    public Single<Pages> getPage(String category){
        return apiInterface.getPage(category);
    }

}
