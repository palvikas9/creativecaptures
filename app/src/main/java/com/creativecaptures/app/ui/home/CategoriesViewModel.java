package com.creativecaptures.app.ui.home;

import android.os.Bundle;

import com.creativecaptures.app.data.model.Category;
import com.creativecaptures.app.data.rest.ApiRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;

public class CategoriesViewModel extends ViewModel {

    private CompositeDisposable disposable;
    private MutableLiveData<List<Category>> categoryList = new MutableLiveData<>();
    private MutableLiveData<Boolean> loading = new MutableLiveData<>();
    private MutableLiveData<Boolean> error = new MutableLiveData<>();

    private ApiRepository apiRepository;

    @Inject
    public CategoriesViewModel(ApiRepository apiRepository){
//        this.apiRepository = apiRepository;
        disposable = new CompositeDisposable();
        fetchCategories();
    }

    public LiveData<List<Category>> getCategory(){
        List<Category> categories = new Gson().fromJson(loadJSONFromAsset(), new TypeToken<List<Category>>(){}.getType());
        loading.setValue(false);
        categoryList.setValue(categories);
        error.setValue(false);
        return categoryList;

    }
    LiveData<Boolean> getError() {
        return error;
    }
    LiveData<Boolean> getLoading() {
        return loading;
    }

    public void fetchCategories(){
        loading.setValue(true);
        /*disposable.add(apiRepository.getCategories().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<List<Category>>(){
                    @Override
                    public void onSuccess(List<Category> value) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        List<Category> categories = new Gson().fromJson(loadJSONFromAsset(), new TypeToken<List<Category>>(){}.getType());
                        loading.setValue(false);
                        categoryList.setValue(categories);
                        error.setValue(false);
                    }
                }));*/

    }

    public void saveToBundle(Bundle outState) {

    }

    public void restoreFromBundle(Bundle savedInstanceState) {

    }

    public String loadJSONFromAsset() {
        String json = "[\n" +
                "  {\n" +
                "    \"id\":\"100\",\n" +
                "    \"name\":\"Baby Shoot\",\n" +
                "    \"categorySlug\":\"baby-shoot\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/April-27-2017-Baby-shoot-115-1024x703.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/April-27-2017-Baby-shoot-115-1024x703.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"101\",\n" +
                "    \"name\":\"Birthday Event\",\n" +
                "    \"categorySlug\":\"birthday-event\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-37-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-37-1024x683.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"103\",\n" +
                "    \"name\":\"Fashion\",\n" +
                "    \"categorySlug\":\"fashion-2\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-2-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-2-1024x683.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"104\",\n" +
                "    \"name\":\"Interior Shoot\",\n" +
                "    \"categorySlug\":\"interior-shoot-2\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-45-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-45-1024x683.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"105\",\n" +
                "    \"name\":\"Maternity Shoot\",\n" +
                "    \"categorySlug\":\"maternity-shoot-2\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-13-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-13-1024x683.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"106\",\n" +
                "    \"name\":\"Wedding\",\n" +
                "    \"categorySlug\":\"wedding\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/kanjin-1-15-1-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/kanjin-1-15-1-1024x683.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"107\",\n" +
                "    \"name\":\"Pre-Wedding Shoot\",\n" +
                "    \"categorySlug\":\"pre-wedding-3\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/December-03-2017-Pre-Wedding-46-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/December-03-2017-Pre-Wedding-46-1024x683.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"108\",\n" +
                "    \"name\":\"Product Shoot\",\n" +
                "    \"categorySlug\":\"product-shoot-2\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-8-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-8-1024x683.jpg\"\n" +
                "  }\n" +
                "]\n";
        return json;
    }
}
