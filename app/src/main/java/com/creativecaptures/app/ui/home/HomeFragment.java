package com.creativecaptures.app.ui.home;

import android.os.Bundle;
import android.view.View;

import com.creativecaptures.app.R;
import com.creativecaptures.app.base.BaseFragment;
import com.creativecaptures.app.di.util.ViewModelFactory;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

public class HomeFragment extends BaseFragment {

    @Inject
    ViewModelFactory viewModelFactory;
    private HomeViewModel homeViewModel;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    protected int layoutRes() {
        return R.layout.home_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel.class);

    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}
