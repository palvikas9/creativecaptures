package com.creativecaptures.app.ui.category;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.creativecaptures.app.R;
import com.creativecaptures.app.base.BaseActivity;
import com.creativecaptures.app.data.model.Category;
import com.creativecaptures.app.di.util.GlideApp;
import com.creativecaptures.app.di.util.ViewModelFactory;
import com.creativecaptures.app.ui.utils.Constants;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import butterknife.BindView;
import butterknife.OnClick;

public class SingleCategoryActivity extends BaseActivity implements ImagesClickListener {

    @BindView(R.id.recyclerViewImages)
    RecyclerView recyclerViewImages;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.noInternetConnection)
    LinearLayout noInternetConnection;

    @BindView(R.id.buttonRetry)
    Button buttonRetry;


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    MaterialDialog dialog;

   /* @BindView(R.id.toolbarImageView)
    AppCompatImageView toolbarImageView;*/

    /*@BindView(R.id.collapsingToolbarLayout)
    CollapsingToolbarLayout collapsingToolbarLayout;*/
    @Inject
    ViewModelFactory viewModelFactory;
    private ImagesAdapter imagesAdapter;
    //    private ImagesClickListener imagesClickListener;
    private StaggeredGridLayoutManager mStaggeredGridLayout;
    private SingleCategoryViewModel mSingleCategoryViewModel;

    @Override
    protected int layoutRes() {
        return R.layout.single_category_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String src = getIntent().getStringExtra("src");
        String name = getIntent().getStringExtra("name");

        /*collapsingToolbarLayout.setContentScrimColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimaryDark));
        collapsingToolbarLayout.setTitle(name);*/
        setSupportActionBar(toolbar);
        setTitle(name);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        mSingleCategoryViewModel = ViewModelProviders.of(this, viewModelFactory).get(SingleCategoryViewModel.class);
        mSingleCategoryViewModel.restoreFromBundle(savedInstanceState);
        mSingleCategoryViewModel.setCategorySlug(src);

        mStaggeredGridLayout = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerViewImages.setLayoutManager(mStaggeredGridLayout);

        imagesAdapter = new ImagesAdapter(this, this);
        mSingleCategoryViewModel.itemPagedList.observe(this, items -> {
                imagesAdapter.submitList(items);
        });

        recyclerViewImages.setNestedScrollingEnabled(false);
        recyclerViewImages.setHasFixedSize(true);
        recyclerViewImages.setItemViewCacheSize(20);
        recyclerViewImages.setDrawingCacheEnabled(true);
        recyclerViewImages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerViewImages.setAdapter(imagesAdapter);

        dialog = new MaterialDialog.Builder(this)
                .title("Getting image for Sharing...")
                .content("Please wait")
                .progress(true, 0)
                .build();

        if(Constants.isOnline(getApplicationContext())) {
            recyclerViewImages.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            noInternetConnection.setVisibility(View.GONE);
        } else {
            noInternetConnection.setVisibility(View.VISIBLE);
            recyclerViewImages.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.buttonRetry)
    public void setButtonRetry(){
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    /**
     * Initialize the contents of the Activity's standard options menu.  You
     * should place your menu items in to <var>menu</var>.
     *
     * <p>This is only called once, the first time the options menu is
     * displayed.  To update the menu every time it is displayed, see
     * {@link #onPrepareOptionsMenu}.
     *
     * <p>The default implementation populates the menu with standard system
     * menu items.  These are placed in the {@link Menu#CATEGORY_SYSTEM} group so that
     * they will be correctly ordered with application-defined menu items.
     * Deriving classes should always call through to the base implementation.
     *
     * <p>You can safely hold on to <var>menu</var> (and any items created
     * from it), making modifications to it as desired, until the next
     * time onCreateOptionsMenu() is called.
     *
     * <p>When you add items to the menu, you can implement the Activity's
     * {@link #onOptionsItemSelected} method to handle them there.
     *
     * @param menu The options menu in which you place your items.
     * @return You must return true for the menu to be displayed;
     * if you return false it will not be shown.
     * @see #onPrepareOptionsMenu
     * @see #onOptionsItemSelected
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Prepare the Screen's standard options menu to be displayed.  This is
     * called right before the menu is shown, every time it is shown.  You can
     * use this method to efficiently enable/disable items or otherwise
     * dynamically modify the contents.
     *
     * <p>The default implementation updates the system menu items based on the
     * activity's state.  Deriving classes should always call through to the
     * base class implementation.
     *
     * @param menu The options menu as last shown or first initialized by
     *             onCreateOptionsMenu().
     * @return You must return true for the menu to be displayed;
     * if you return false it will not be shown.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.</p>
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * ObserveViewModel function is required in the app in order to observe on the values being fetched
     * from the database or any other source on which the listening is being done in the application
     * <p>
     * This function is mainly responsible in the application for showing the current health of the
     * user in accordance with the data entered by the user in the app's profile page
     */
    private void observeViewModel() {

        mSingleCategoryViewModel.getLoading().observe(this, isLoading -> {
            if (isLoading != null) {
                if (isLoading) {

                } else {

                }
            }
        });

        mSingleCategoryViewModel.getError().observe(this, isError -> {
            if (isError != null) {
                if (isError) {

                } else {

                }
            }
        });

        mSingleCategoryViewModel.getCategory().observe(this, categories -> {
            if (categories != null) {
                recyclerViewImages.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onImageClick(Category images) {
        Bundle b = new Bundle();
        b.putString("src", images.getSrc());

        FullScreenDialog dialog = new FullScreenDialog();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        dialog.setArguments(b);
        dialog.show(ft, FullScreenDialog.TAG);
    }

    @Override
    public void onShareClick(Category images, AppCompatImageView imageView) {
        dialog.show();
        GlideApp.with(getApplicationContext())
                .asFile()
                .onlyRetrieveFromCache(false)
                .load(images.getSrcThumb())
                .listener(new RequestListener<File>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<File> target, boolean isFirstResource) {
                        dialog.dismiss();
                        Snackbar.make(imageView, "Something went wrong", Snackbar.LENGTH_LONG).show();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(File resource, Object model, Target<File> target, DataSource dataSource, boolean isFirstResource) {
                        File from = new File(resource.getAbsolutePath());
                        File to = new File(resource.getParent(), Long.toString(System.currentTimeMillis()) + ".jpg");
                        from.renameTo(to);
                        dialog.dismiss();
                        Uri uri = FileProvider.getUriForFile(getApplicationContext(), getApplication().getPackageName() + ".provider", to);
                        share(uri); // startActivity probably needs UI thread
                        return false;
                    }
                }).submit();
    }

    private void share(Uri result) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Share image");
        intent.putExtra(Intent.EXTRA_TEXT, "Captured by Creative Capture");
        intent.putExtra(Intent.EXTRA_STREAM, result);
        startActivity(Intent.createChooser(intent, "Share image"));
    }
}
