package com.creativecaptures.app.ui.home;

import com.creativecaptures.app.data.model.Category;
import com.creativecaptures.app.data.rest.ApiRepository;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;

public class HomeViewModel extends ViewModel {

    private CompositeDisposable disposable;
    private MutableLiveData<List<Category>> categoryList = new MutableLiveData<>();
    private MutableLiveData<Boolean> loading = new MutableLiveData<>();
    private MutableLiveData<Boolean> error = new MutableLiveData<>();

    private ApiRepository apiRepository;

    @Inject
    public HomeViewModel(ApiRepository apiRepository){
        this.apiRepository = apiRepository;
        disposable = new CompositeDisposable();
    }

}
