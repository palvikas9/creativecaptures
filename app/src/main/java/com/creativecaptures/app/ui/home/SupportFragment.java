package com.creativecaptures.app.ui.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.creativecaptures.app.R;
import com.creativecaptures.app.base.BaseFragment;
import com.creativecaptures.app.data.model.SupportModel;
import com.creativecaptures.app.di.util.ViewModelFactory;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

public class SupportFragment extends BaseFragment implements SupportSelectedListener {

    @BindView(R.id.recyclerViewContacts)
    RecyclerView recyclerViewContacts;

    @BindView(R.id.floatingActionCall)
    FloatingActionButton floatingActionButton;

    @Inject
    ViewModelFactory viewModelFactory;
    private SupportViewModel mViewModel;

    public SupportFragment() {

    }

    @Override
    protected int layoutRes() {
        return R.layout.contact_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(SupportViewModel.class);

        recyclerViewContacts.setLayoutManager(new LinearLayoutManager(getBaseActivity().getApplicationContext()));
        recyclerViewContacts.setAdapter(new SupportAdapter(getBaseActivity().getApplicationContext(),this, mViewModel, this));

        observeViewModel();
    }

    public void observeViewModel(){
        mViewModel.getContacts().observe(this, values->{
            if(values != null) {
                recyclerViewContacts.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onSupportClicked(SupportModel supportModel) {
        if(supportModel.getId() == 2) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:"+getString(R.string.text_phone)));
            startActivity(intent);
        } else if(supportModel.getId() == 3) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("*/*");
            intent.putExtra(Intent.EXTRA_CC, new String[]{ "vivekchawla725@gmail.com"});
            intent.putExtra(Intent.EXTRA_EMAIL,new String[]{"info@creativecapture.co.in"});
            intent.putExtra(Intent.EXTRA_SUBJECT,"Contact us");
            intent.putExtra(Intent.EXTRA_TEXT,"Dear Creative Capture,\n");
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
            else{
                Toast.makeText(getActivity(), "No default email applications found...", Toast.LENGTH_SHORT).show();
            }
        }else if (supportModel.getId() == 4) {
            String url = "http://creativecapture.co.in/";
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(getActivity(), Uri.parse(url));
        }
    }

    @OnClick(R.id.floatingActionCall)
    public void callToAction() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+getString(R.string.text_phone)));
        startActivity(intent);
    }
}
