package com.creativecaptures.app.ui.category;

import com.creativecaptures.app.data.model.Category;

import androidx.appcompat.widget.AppCompatImageView;

public interface ImagesClickListener {
    void onImageClick(Category images);

    void onShareClick(Category images, AppCompatImageView imageView);
}
