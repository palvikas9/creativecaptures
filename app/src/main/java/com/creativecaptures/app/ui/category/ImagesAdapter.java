package com.creativecaptures.app.ui.category;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.creativecaptures.app.R;
import com.creativecaptures.app.data.model.Category;
import com.creativecaptures.app.di.util.GlideApp;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ImagesAdapter extends PagedListAdapter<Category, ImagesAdapter.CategoriesViewHolder> {

    /**
     * @author vpal
     * @date 19-03-2019
     * @use This is used in the application in order to check the difference between the previous item
     * and current item so that there is no difference and same items can be updated in the application
     */
    private static DiffUtil.ItemCallback<Category> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Category>() {
                @Override
                public boolean areItemsTheSame(Category oldItem, Category newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @Override
                public boolean areContentsTheSame(Category oldItem, Category newItem) {
                    return oldItem.equals(newItem);
                }
            };

    private Context mContext;
    private ImagesClickListener mImagesClickListener;

    ImagesAdapter(Context context, ImagesClickListener imagesClickListener) {
        super(DIFF_CALLBACK);
        this.mContext = context;
        this.mImagesClickListener = imagesClickListener;
    }

    @NonNull
    @Override
    public ImagesAdapter.CategoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_images, parent, false);
        return new ImagesAdapter.CategoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImagesAdapter.CategoriesViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    public class CategoriesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon_frame)
        FrameLayout cardViewCategory;

        @BindView(R.id.imageViewCategory)
        AppCompatImageView imageViewCategory;

        @BindView(R.id.textViewCategory)
        AppCompatTextView textViewCategory;

        @BindView(R.id.fabShare)
        FloatingActionButton fabShare;

        @BindView(R.id.progressImages)
        ProgressBar progressBarImages;

        public CategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Category category) {
            GlideApp.with(mContext)
                    .load(category.getSrcThumb())
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            imageViewCategory.setImageResource(R.drawable.ic_header_new);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            imageViewCategory.setImageDrawable(resource);
                            progressBarImages.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(imageViewCategory);

            textViewCategory.setText(category.getName());

            cardViewCategory.setOnClickListener(v -> {
                mImagesClickListener.onImageClick(category);
            });

            fabShare.setOnClickListener(v-> {
                mImagesClickListener.onShareClick(category,imageViewCategory);
            });

        }
    }
}
