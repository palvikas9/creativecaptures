package com.creativecaptures.app.ui.category;

import com.creativecaptures.app.data.model.Category;
import com.creativecaptures.app.data.rest.ApiRepository;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;
import io.reactivex.disposables.CompositeDisposable;

public class CategoryDataSourceFactory extends DataSource.Factory<Integer,Category> {
 
    //creating the mutable live data
    private MutableLiveData<PageKeyedDataSource<Integer, Category>> itemLiveDataSource = new MutableLiveData<>();
    private ApiRepository apiRepository;
    private CompositeDisposable compositeDisposable;
    private String categorySlug;

    public CategoryDataSourceFactory(ApiRepository repository, CompositeDisposable compositeDisposable, String categorySlug) {
        this.apiRepository = repository;
        this.compositeDisposable = compositeDisposable;
        itemLiveDataSource = new MutableLiveData<>();
        this.categorySlug = categorySlug;
    }

    @Override
    public DataSource<Integer, Category> create() {
        //getting our data source object
        CategoryDataSource itemDataSource = new CategoryDataSource(apiRepository,compositeDisposable,categorySlug);

        //posting the datasource to get the values
        itemLiveDataSource.postValue(itemDataSource);
        
        //returning the datasource
        return itemDataSource;
    }
 
    
    //getter for itemlivedatasource
    public MutableLiveData<PageKeyedDataSource<Integer, Category>> getItemLiveDataSource() {
        return itemLiveDataSource;
    }
}