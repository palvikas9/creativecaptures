package com.creativecaptures.app.ui.splash;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.creativecaptures.app.R;
import com.creativecaptures.app.base.BaseActivity;
import com.creativecaptures.app.ui.home.HomeActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import butterknife.BindView;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.imageViewSplash)
    AppCompatImageView imageViewSplash;

    PermissionListener permissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted(PermissionGrantedResponse response) {
            new Handler().postDelayed(() -> {
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(SplashActivity.this,
                                imageViewSplash,
                                ViewCompat.getTransitionName(imageViewSplash));
                startActivity(intent, options.toBundle());
                SplashActivity.this.finish();
            }, 2000);

        }

        @Override
        public void onPermissionDenied(PermissionDeniedResponse response) {

        }

        @Override
        public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

        }
    };

    @Override
    protected int layoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(permissionListener)
                .onSameThread()
                .check();
    }

}
