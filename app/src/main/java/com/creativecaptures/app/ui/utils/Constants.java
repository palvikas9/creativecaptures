package com.creativecaptures.app.ui.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Constants {
    public static final String ASSETS_DIR_FONTS = "fonts/";
    public static final String ASSETS_DIR_JSON = "json/";
    public static final String ASSETS_FILE_FONTS = ASSETS_DIR_JSON + "fonts.json";

    public static final String APP_CUSTOM_LIGHT_FONT_FILE_NAME = "DIN-Light.otf";
    public static final String APP_CUSTOM_MEDIUM_FILE_NAME = "DIN-Medium.otf";
    public static final String APP_CUSTOM_REGULAR_FONT_FILE_NAME = "DIN-Regular.otf";
    public static final String APP_CUSTOM_BOLD_FONT_FILE_NAME = "DIN-Bold.otf";
    public static final String APP_CUSTOM_COCOGOOSE = "Sen.otf";
//    public static final String APP_CUSTOM_FONT_FILE_NAME = APP_CUSTOM_REGULAR_FONT_FILE_NAME;
    public static final String APP_CUSTOM_FONT_FILE_NAME = APP_CUSTOM_COCOGOOSE;
    public static final String DEFAULT_FONT_NAME_TO_OVERRIDE = "SERIF";

    /**
     * Is online boolean.
     *
     * @param context the context
     * @return the boolean
     */
    public static boolean isOnline(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr != null ? connMgr.getActiveNetworkInfo() : null;
        return (networkInfo != null && networkInfo.isConnected());
    }
}
