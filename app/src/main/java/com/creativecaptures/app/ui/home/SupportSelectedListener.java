package com.creativecaptures.app.ui.home;


import com.creativecaptures.app.data.model.SupportModel;

public interface SupportSelectedListener {
    void onSupportClicked(SupportModel supportModel);
}
