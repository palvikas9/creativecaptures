package com.creativecaptures.app.ui.home;

import com.creativecaptures.app.data.model.Category;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

public interface CategoryClickHandler {
    /**
     * This is used to listen to the click on the category items loaded in the recycler view
     *
     * @param category - the category on which the click happened
     * @param imageView - the imageview used for transition
     * @param appCompatTextView - the category name used for transition
     */
    void onCategoryClick(Category category, AppCompatImageView imageView, AppCompatTextView appCompatTextView);
}
