package com.creativecaptures.app.ui.category;

import com.creativecaptures.app.data.model.Category;
import com.creativecaptures.app.data.model.Pages;
import com.creativecaptures.app.data.rest.ApiRepository;
import com.creativecaptures.app.ui.utils.StringParserForImages;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CategoryDataSource extends PageKeyedDataSource<Integer, Category> {

    //the size of a page that we want
    public static final int PAGE_SIZE = 10;

    //we will start from the first page which is 1
    private static int FIRST_PAGE = 0;
    int maxKeySize = 1;

    private CompositeDisposable disposable;
    private ApiRepository apiRepository;
    private List<Category> mCategory = new ArrayList<>();
    private String categorySlug;

    public CategoryDataSource(ApiRepository apiRepository, CompositeDisposable disposable, String s) {
        this.apiRepository = apiRepository;
        this.disposable = disposable;
        categorySlug = s;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Category> callback) {
        disposable.add(apiRepository.getPage(categorySlug).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<Pages>(){
                    @Override
                    public void onSuccess(Pages value) {
                        List<Category> categories = StringParserForImages.getImagesFromContent(value.getPage());
                        mCategory.addAll(categories);
                        maxKeySize = mCategory.size()/PAGE_SIZE;
                        if(mCategory.size() >0 && mCategory.size() > PAGE_SIZE) {
                            callback.onResult(mCategory.subList(0, PAGE_SIZE), null, FIRST_PAGE + 1);
                        } else if(mCategory.size() >0 && mCategory.size()< PAGE_SIZE) {
                            callback.onResult(mCategory.subList(0, mCategory.size()), null, FIRST_PAGE + 1);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Category> callback) {
        callback.onResult(null,params.key + 1);
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Category> callback) {

        Integer key = params.key;

        //passing the loaded data and next page value
        if(key < maxKeySize) {
            if (((PAGE_SIZE * key) + PAGE_SIZE) < mCategory.size()) {
                callback.onResult(mCategory.subList(key * PAGE_SIZE, (PAGE_SIZE * key) + PAGE_SIZE), key +1);
            } else {
                callback.onResult(mCategory.subList(key * PAGE_SIZE, mCategory.size()-1), key+1);
            }
        }



    }
}
