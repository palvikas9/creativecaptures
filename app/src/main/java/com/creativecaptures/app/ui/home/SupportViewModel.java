

package com.creativecaptures.app.ui.home;


import com.creativecaptures.app.R;
import com.creativecaptures.app.data.model.SupportModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SupportViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();
    private final MutableLiveData<List<SupportModel>> contacts = new MutableLiveData<>();

    @Inject
    public SupportViewModel(){
        setContacts();
    }

    LiveData<List<SupportModel>> getContacts(){
        return contacts;
    }

    private void setContacts(){
        List<SupportModel> supportModelsList = new ArrayList<>();

        SupportModel supportModelPhone = new SupportModel().setHeading(R.string.text_phone).setDescription(R.string.cc_phone)
                .setIcon(R.drawable.ic_phonelink_ring_white_24dp)
                .setSubDescription(R.string.text_click_to_call)
                .setId(2);

        SupportModel supportModelEmail = new SupportModel().setHeading(R.string.text_email).setDescription(R.string.cc_email)
                .setIcon(R.drawable.ic_email_white_24dp)
                .setSubDescription(R.string.text_click_to_compose)
                .setId(3);

        SupportModel supportModelWebsite = new SupportModel().setHeading(R.string.text_visit_now).setDescription(R.string.cc_visit)
                .setIcon(R.drawable.ic_language_white_24dp)
                .setSubDescription(R.string.text_click_to_buy)
                .setId(4);

        supportModelsList.add(supportModelPhone);
        supportModelsList.add(supportModelEmail);
        supportModelsList.add(supportModelWebsite);

        contacts.postValue(supportModelsList);
    }
}
