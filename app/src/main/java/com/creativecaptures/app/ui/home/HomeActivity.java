package com.creativecaptures.app.ui.home;

import android.os.Bundle;
import android.view.MenuItem;

import com.creativecaptures.app.R;
import com.creativecaptures.app.base.BaseActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;

public class HomeActivity extends BaseActivity {

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_about:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_contact:
                    viewPager.setCurrentItem(2);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected int layoutRes() {
        return R.layout.activity_home;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        bottomNavigationView.setSelectedItemId(R.id.navigation_home);

        viewPager.setAdapter(new HomeViewPager(getSupportFragmentManager()));

        viewPager.setCurrentItem(0);

    }

    private class HomeViewPager extends FragmentPagerAdapter {

        public HomeViewPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 1) {
                return new AboutUsFragment();
            } else if (position == 2) {
                return new SupportFragment();
            } else {
                return new CategoryFragment();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

    }


}
