package com.creativecaptures.app.ui.category;

import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.creativecaptures.app.R;
import com.creativecaptures.app.di.util.GlideApp;
import com.github.chrisbanes.photoview.PhotoView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FullScreenDialog extends DialogFragment {

    public static String TAG = "FullScreenDialog";
    @BindView(R.id.photoView)
    PhotoView photoView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.textViewLoading)
    TextView textViewLoading;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.layout_full_screen_image, container, false);

        ButterKnife.bind(this,view);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle("Viewing in Hi-Res Image");
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(listener -> {
            getDialog().dismiss();
        });

        Bundle b = getArguments();
        String name = b.getString("src", "");

        GlideApp.with(getActivity().getApplicationContext())
                .load(name)
//                .placeholder(R.drawable.ic_header_new)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        photoView.setImageResource(R.drawable.ic_header_new);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        photoView.setImageDrawable(resource);
                        progressBar.setVisibility(View.GONE);
                        textViewLoading.setVisibility(View.GONE);
                        return false;
                    }
                }).into(photoView);

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog !=null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}