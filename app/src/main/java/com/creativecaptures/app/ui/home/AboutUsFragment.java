package com.creativecaptures.app.ui.home;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.creativecaptures.app.R;
import com.creativecaptures.app.base.BaseFragment;
import com.creativecaptures.app.di.util.GlideApp;
import com.creativecaptures.app.di.util.ViewModelFactory;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.OnClick;

public class AboutUsFragment extends BaseFragment {

    @BindView(R.id.profilePic)
    AppCompatImageView profilePic;

    @BindView(R.id.textViewPhotos)
    AppCompatTextView textViewPhotos;

    @BindView(R.id.textViewClients)
    AppCompatTextView textViewClients;
    @Inject
    ViewModelFactory viewModelFactory;
    private Context mContext;
    private AboutUsViewModel aboutUsViewModel;


    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    protected int layoutRes() {
        return R.layout.about_us_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        aboutUsViewModel = ViewModelProviders.of(this, viewModelFactory).get(AboutUsViewModel.class);
        mContext = getActivity().getApplicationContext();

        GlideApp.with(mContext)
                .load(R.drawable.ic_profile)
                .circleCrop()
                .into(profilePic);

    }

    @OnClick(R.id.cardViewCall)
    public void callStart(){
        String uri = "tel:" + "7988215860";
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(uri));
        startActivity(intent);
    }

}
