package com.creativecaptures.app.ui.utils;

import com.creativecaptures.app.data.model.Category;
import com.creativecaptures.app.data.model.Page;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class StringParserForImages {

    public static List<Category> getImagesFromContent(Page page) {
        List<Category> imagesList = new ArrayList<>();
        if(page.getSlug().equalsIgnoreCase("birthday-event")) {
            String contentArray[] = page.getContent().split("<dl class='gallery-item'>\\n\\t\\t\\t<dt class='gallery-icon landscape'>\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"ed5691f\\\" href=");
            String contentArrayLandscape[] = page.getContent().split("<dl class='gallery-item'>\\n\\t\\t\\t<dt class='gallery-icon portrait'>\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"ed5691f\\\" href=");


            for (int i = 1; i < contentArray.length - 1; i++) {
                String contentString = contentArray[i];

                String src = StringUtils.substringBetween(contentString, "'", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src).setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(i);

                imagesList.add(category);
            }

            for (int ij = 1; ij < contentArrayLandscape.length - 1; ij++) {
                String contentString = contentArray[ij];

                String src = StringUtils.substringBetween(contentString, "'", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src).setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(ij);

                imagesList.add(category);
            }
        } else if(page.getSlug().equalsIgnoreCase("baby-shoot")) {
            String contentArray[] = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon landscape'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"ad6f575\\\" href=");
            String contentArrayLandscape[] = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon portrait'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"ad6f575\\\" href=");


            for (int i = 1; i < contentArray.length - 1; i++) {
                String contentString = contentArray[i];

                String src = StringUtils.substringBetween(contentString, "'", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-thumbnail size-thumbnail\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(src)
                        /*.setSrcThumb(thumb.replace("\" class=", ""))*/.setName("").setId(i);

                imagesList.add(category);
            }

            for (int ij = 1; ij < contentArrayLandscape.length - 1; ij++) {
                String contentString = contentArrayLandscape[ij];

                String src = StringUtils.substringBetween(contentString, "'", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-thumbnail size-thumbnail\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(src)
                        /*.setSrcThumb(thumb.replace("\" class=", ""))*/.setName("").setId(ij);
                imagesList.add(category);
            }
        } else if(page.getSlug().equalsIgnoreCase("fashion-2")) {

            String contentArray[] = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon landscape'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"66ae0e4\\\" ");
            String contentArrayLandscape[] = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon portrait'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"66ae0e4\\\" ");


            for (int i = 1; i < contentArray.length - 1; i++) {
                String contentString = contentArray[i];

                String src = StringUtils.substringBetween(contentString, "href='", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(i);

                imagesList.add(category);
            }
            for (int ij = 1; ij < contentArrayLandscape.length - 1; ij++) {
                String contentString = contentArrayLandscape[ij];

                String src = StringUtils.substringBetween(contentString, "href='", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(ij);

                imagesList.add(category);
            }
        } else if(page.getSlug().equalsIgnoreCase("interior-shoot-2")) {
            String contentArray[] = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon landscape'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"6b493a6\\\" ");
            String contentArrayLandscape[] = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon portrait'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"6b493a6\\\" ");


            for (int i = 1; i < contentArray.length - 1; i++) {
                String contentString = contentArray[i];

                String src = StringUtils.substringBetween(contentString, "href='", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(i);

                imagesList.add(category);
            }

            for (int i = 1; i < contentArrayLandscape.length - 1; i++) {
                String contentString = contentArrayLandscape[i];

                String src = StringUtils.substringBetween(contentString, "href='", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(i);

                imagesList.add(category);
            }
        } else if(page.getSlug().equalsIgnoreCase("maternity-shoot-2")) {
            String contentArray[] = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon landscape'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"2dcfa08\\\" ");
            String contentArrayLandscape[] = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon portrait'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"2dcfa08\\\" ");



            for (int i = 1; i < contentArray.length - 1; i++) {
                String contentString = contentArray[i];

                String src = StringUtils.substringBetween(contentString, "href='", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(i);

                imagesList.add(category);
            }

            for (int j = 1; j < contentArrayLandscape.length - 1; j++) {
                String contentString = contentArrayLandscape[j];

                String src = StringUtils.substringBetween(contentString, "href='", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(j);

                imagesList.add(category);
            }
        } else if(page.getSlug().equalsIgnoreCase("pre-wedding-3")) {
            String contentArray[] = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon portrait'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"623207e\\\" ");
            String contentArrayLandscape[] = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon landscape'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"623207e\\\" ");



            for (int i = 1; i < contentArray.length - 1; i++) {
                String contentString = contentArray[i];

                String src = StringUtils.substringBetween(contentString, "href='", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(i);

                imagesList.add(category);
            }

            for (int j = 1; j < contentArrayLandscape.length - 1; j++) {
                String contentString = contentArrayLandscape[j];

                String src = StringUtils.substringBetween(contentString, "href='", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(j);

                imagesList.add(category);
            }
        } else if(page.getSlug().equalsIgnoreCase("product-shoot-2")) {
            String[] contentArray = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon landscape'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"70cc1b0\\\" ");
            String[] contentArrayLandscape = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon portrait'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"623207e\\\" ");



            for (int i = 1; i < contentArray.length - 1; i++) {
                String contentString = contentArray[i];

                String src = StringUtils.substringBetween(contentString, "href='", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(i);

                imagesList.add(category);
            }

            for (int j = 1; j < contentArrayLandscape.length - 1; j++) {
                String contentString = contentArrayLandscape[j];

                String src = StringUtils.substringBetween(contentString, "href='", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(j);

                imagesList.add(category);
            }
        } else if(page.getSlug().equalsIgnoreCase("wedding")) {
            String contentArray[] = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon landscape'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"42f41be\\\" ");
            String contentArrayLandscape[] = page.getContent().split("<dl class='gallery-item'>" +
                    "\\n\\t\\t\\t" +
                    "<dt class='gallery-icon portrait'>" +
                    "\\n\\t\\t\\t\\t<a data-elementor-open-lightbox=\\\"default\\\" data-elementor-lightbox-slideshow=\\\"42f41be\\\" ");



            for (int i = 1; i < contentArray.length - 1; i++) {
                String contentString = contentArray[i];

                String src = StringUtils.substringBetween(contentString, "href='", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(i);

                imagesList.add(category);
            }

            for (int j = 1; j < contentArrayLandscape.length - 1; j++) {
                String contentString = contentArrayLandscape[j];

                String src = StringUtils.substringBetween(contentString, "href='", "'><img");

                String thumb = StringUtils.substringBetween(contentString, "src=\"", "attachment-large size-large\"");

                Category category = new Category().setSrc(src)
                        .setSrcThumb(thumb.replace("\" class=", "")).setName("").setId(j);

                imagesList.add(category);
            }
        }

        return imagesList;
    }

}
