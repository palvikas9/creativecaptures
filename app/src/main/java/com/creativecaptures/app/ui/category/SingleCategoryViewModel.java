package com.creativecaptures.app.ui.category;

import android.os.Bundle;

import com.creativecaptures.app.data.model.Category;
import com.creativecaptures.app.data.model.Pages;
import com.creativecaptures.app.data.rest.ApiRepository;
import com.creativecaptures.app.ui.utils.StringParserForImages;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SingleCategoryViewModel extends ViewModel {

    LiveData<PagedList<Category>> itemPagedList;
    LiveData<PageKeyedDataSource<Integer, Category>> liveDataSource;
    private CompositeDisposable disposable;
    private MutableLiveData<List<Category>> categoryList = new MutableLiveData<>();
    private MutableLiveData<Boolean> loading = new MutableLiveData<>();
    private MutableLiveData<Boolean> error = new MutableLiveData<>();
    private String categorySlug = "birthday-event";
    private ApiRepository apiRepository;

    @Inject
    public SingleCategoryViewModel(ApiRepository apiRepository){
        this.apiRepository = apiRepository;
        disposable = new CompositeDisposable();
//        fetchCategories();
    }

    public void setCategorySlug(String slug) {

        CategoryDataSourceFactory itemDataSourceFactory = new CategoryDataSourceFactory(apiRepository,disposable,slug);

        //getting the live data source from data source factory
        liveDataSource = itemDataSourceFactory.getItemLiveDataSource();

        //Getting PagedList config
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setPageSize(CategoryDataSource.PAGE_SIZE).build();

        //Building the paged list
        itemPagedList = (new LivePagedListBuilder(itemDataSourceFactory, pagedListConfig)).build();
    }

    LiveData<List<Category>> getCategory(){
        return categoryList;
    }
    LiveData<Boolean> getError() {
        return error;
    }
    LiveData<Boolean> getLoading() {
        return loading;
    }

    public void fetchCategories(){
        loading.setValue(true);
        disposable.add(apiRepository.getPage(categorySlug).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<Pages>(){
                    @Override
                    public void onSuccess(Pages value) {
                        List<Category> categories = StringParserForImages.getImagesFromContent(value.getPage());
                        error.setValue(false);
                        loading.setValue(false);
                        categoryList.setValue(categories.subList(0,10));
                    }

                    @Override
                    public void onError(Throwable e) {
                        List<Category> categories = new Gson().fromJson(loadJSONFromAsset(), new TypeToken<List<Category>>(){}.getType());
                        loading.setValue(false);
                        categoryList.setValue(categories);
                        error.setValue(false);
                    }
                }));
    }

    public void saveToBundle(Bundle outState) {

    }

    public void restoreFromBundle(Bundle savedInstanceState) {

    }

    public String loadJSONFromAsset() {
        String json = "[\n" +
                "  {\n" +
                "    \"id\":\"100\",\n" +
                "    \"name\":\"Baby Shoot\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/April-27-2017-Baby-shoot-115-1024x703.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/April-27-2017-Baby-shoot-115-1024x703.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"101\",\n" +
                "    \"name\":\"Birthday Event\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-37-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-37-1024x683.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"102\",\n" +
                "    \"name\":\"Cinematography\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/movie-2545676_640.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/movie-2545676_640.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"103\",\n" +
                "    \"name\":\"Fashion\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-2-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-2-1024x683.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"104\",\n" +
                "    \"name\":\"Interior Shoot\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-45-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-45-1024x683.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"105\",\n" +
                "    \"name\":\"Maternity Shoot\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-13-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-13-1024x683.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"106\",\n" +
                "    \"name\":\"Wedding\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/kanjin-1-15-1-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/kanjin-1-15-1-1024x683.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"107\",\n" +
                "    \"name\":\"Pre-Wedding Shoot\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/December-03-2017-Pre-Wedding-46-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/December-03-2017-Pre-Wedding-46-1024x683.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\":\"108\",\n" +
                "    \"name\":\"Pre-Wedding Shoot\",\n" +
                "    \"src\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-8-1024x683.jpg\",\n" +
                "    \"src_thumb\": \"http://creativecapture.co.in/wp-content/uploads/2018/11/1-8-1024x683.jpg\"\n" +
                "  }\n" +
                "]\n";
        return json;
    }
}
