package com.creativecaptures.app.ui.home;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.creativecaptures.app.R;
import com.creativecaptures.app.base.BaseFragment;
import com.creativecaptures.app.data.model.Category;
import com.creativecaptures.app.di.util.ViewModelFactory;
import com.creativecaptures.app.ui.category.SingleCategoryActivity;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends BaseFragment implements CategoryClickHandler{

    @BindView(R.id.recyclerViewCategory)
    RecyclerView recyclerViewCategory;

    @BindView(R.id.relativeLayoutEmpty)
    RelativeLayout relativeLayoutEmpty;

    @BindView(R.id.relativeLayoutError)
    RelativeLayout relativeLayoutError;

    @Inject
    ViewModelFactory viewModelFactory;
    private CategoriesViewModel categoriesViewModel;

    public CategoryFragment() {
        // Required empty public constructor
    }


    @Override
    protected int layoutRes() {
        return R.layout.fragment_category;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        categoriesViewModel = ViewModelProviders.of(this, viewModelFactory).get(CategoriesViewModel.class);
        categoriesViewModel.restoreFromBundle(savedInstanceState);

        recyclerViewCategory.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));
        recyclerViewCategory.setAdapter(new CategoriesAdapter(this,categoriesViewModel,getContext(),
                this));
        observeViewModel();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * ObserveViewModel function is required in the app in order to observe on the values being fetched
     * from the database or any other source on which the listening is being done in the application
     * <p>
     * This function is mainly responsible in the application for showing the current health of the
     * user in accordance with the data entered by the user in the app's profile page
     */
    private void observeViewModel() {
        categoriesViewModel.getCategory().observe(this, categories -> {
            if(categories != null) {
                recyclerViewCategory.setVisibility(View.VISIBLE);
            }
        });

        categoriesViewModel.getLoading().observe(this, isLoading -> {
            if (isLoading != null) {
                relativeLayoutEmpty.setVisibility(isLoading ? View.VISIBLE : View.GONE);
                if (isLoading) {
                    relativeLayoutError.setVisibility(View.GONE);
                    recyclerViewCategory.setVisibility(View.GONE);
                }
            }
        });

        categoriesViewModel.getError().observe(this, isError -> {
            if (isError != null) if(isError) {
                relativeLayoutError.setVisibility(View.VISIBLE);
                recyclerViewCategory.setVisibility(View.GONE);
//                errorTextView.setText("An Error Occurred While Loading Data!");
            }else {
                relativeLayoutError.setVisibility(View.GONE);
//                errorTextView.setText(null);
            }
        });
    }

    /**
     * This is used to listen to the click on the category items loaded in the recycler view
     *
     * @param category          - the category on which the click happened
     * @param imageView         - the imageview used for transition
     * @param appCompatTextView - the category name used for transition
     */
    @Override
    public void onCategoryClick(Category category, AppCompatImageView imageView, AppCompatTextView appCompatTextView) {
        Intent intentCategory = new Intent(getActivity(), SingleCategoryActivity.class);
        intentCategory.putExtra("src", category.getCategorySlug());
        intentCategory.putExtra("name", category.getName());
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(getActivity(), imageView, "imageview_category" );
        startActivity(intentCategory, options.toBundle());

    }
    /** EOC **/
}
