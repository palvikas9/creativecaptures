package com.creativecaptures.app.ui.home;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class CategoryFragmentBindingModule {

    @ContributesAndroidInjector
    abstract CategoryFragment providesCategoryFragment();

    @ContributesAndroidInjector
    abstract AboutUsFragment providesAboutUsFragment();

    @ContributesAndroidInjector
    abstract SupportFragment provideSupportFragment();

}
