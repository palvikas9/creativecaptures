package com.creativecaptures.app.ui.home;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.creativecaptures.app.R;
import com.creativecaptures.app.data.model.Category;
import com.creativecaptures.app.di.util.GlideApp;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder> {

    private final List<Category> data = new ArrayList<>();
    private Context mContext;
    private CategoryClickHandler mCategoryClickHandler;

    public CategoriesAdapter(LifecycleOwner lifecycleOwner, CategoriesViewModel categoriesViewModel, Context context,
                             CategoryClickHandler categoryClickHandler) {
        this.mContext = context;
        categoriesViewModel.getCategory().observe(lifecycleOwner, categories-> {
            if(data.size() == 0) {
                if (categories != null) {
                    data.clear();
                    data.addAll(categories);
                    notifyDataSetChanged();
                }
            }
        });
        this.mCategoryClickHandler = categoryClickHandler;
    }

    @NonNull
    @Override
    public CategoriesAdapter.CategoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        return new CategoriesAdapter.CategoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class CategoriesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cardViewCategory)
        CardView cardViewCategory;

        @BindView(R.id.imageViewCategory)
        AppCompatImageView imageViewCategory;

        @BindView(R.id.textViewCategory)
        AppCompatTextView textViewCategory;

        public CategoriesViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        void bind(Category category) {
            GlideApp.with(mContext)
                    .load(category.getSrc())
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            imageViewCategory.setImageDrawable(resource);
                            Bitmap bitmap = ((BitmapDrawable) imageViewCategory.getDrawable()).getBitmap();

                            if(bitmap != null) {
                                Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                                    public void onGenerated(Palette palette) {
                                        textViewCategory.setTextColor(palette.getDarkVibrantColor(ContextCompat.getColor(mContext,R.color.md_black_1000)));
                                    }
                                });
                            }
                            return false;
                        }
                    }).into(imageViewCategory);
            textViewCategory.setText(category.getName());

            cardViewCategory.setOnClickListener(v->{
                mCategoryClickHandler.onCategoryClick(category,imageViewCategory,textViewCategory);
            });
        }
    }
}
